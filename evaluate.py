import yaml
import pandas as pd
from datetime import datetime
from joblib import load
from sklearn.metrics import mean_squared_error

config_path = 'params/evaluate.yml'


def make_test_report(X, y_true, model, config):
    score = model.score(X, y_true)
    y_pred = model.predict(X)
    mse = mean_squared_error(y_true, y_pred, squared=True)
    report = [
            f'Training data time: {datetime.now()}\n',
            f'Training data len: {len(X)}\n',
            f'R^2 is: {score:.4f}\n',
            f'MSE is {mse:.4f}\n'
            ]
    with open(config['report_path'], 'w', encoding='utf-8') as f:
        f.writelines(report)


if __name__ == "__main__":
    with open(config_path) as file:
        config = yaml.safe_load(file)

    model = load(config['model_path'])
    test_df = pd.read_csv(config['test_df'])
    X = test_df['total_meters'].to_numpy().reshape(-1, 1)
    y_true = test_df['price']
    make_test_report(X, y_true, model, config)
