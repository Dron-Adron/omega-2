import pandas as pd
import yaml
from sklearn.model_selection import train_test_split


config_path = "params/preprocess_cian.yaml"


def read_cian_data(config):
    in_data = config['in_data']
    df = pd.read_csv(in_data, sep=';')
    new_df = df[['total_meters', 'price']]
    return new_df

if __name__ == "__main__":
    with open(config_path) as f:
        config = yaml.safe_load(f)
    df = read_cian_data(config)
    df['price'] = df['price'] / 1000
    df = df.drop(df[df.total_meters < 0].index)
    
    train_df, test_df = train_test_split(df, test_size=0.2)
    train_df.to_csv(config['train_out'])
    test_df.to_csv(config['test_out'])
