import pandas as pd
import click
import yaml
from sklearn.model_selection import train_test_split

@click.command()
@click.option('--in_data', default='data/raw/kaggle/train.csv')
@click.option('--out_data', default='data/processed/train.csv')
@click.option('--config', default='params/data.yml')
def preprocess(in_data, out_data, config):
    with open(config) as f:
        config = yaml.safe_load(f)
    df = pd.read_csv("/home/finu/uni_project/data/row/kaggle/train.csv")
    df = df.rename(columns={'GrLivArea':'total_meters', 'SalePrice':'price'})

    work_cols = df[['total_meters', 'price']]
    df['total_meters'] = work_cols.loc[:, 'total_meters'] = work_cols['total_meters'].apply(
        lambda x: int(round(x * 0.3048)))
    df['price'] = work_cols.loc[:, 'price'] = work_cols['price'].apply(
        lambda x: int(round(x * 80)))

    train_df, test_df = train_test_split(df, test_size=0.2)
    train_df.to_csv(config['train_out'])
    test_df.to_csv(config['test_out'])


if __name__ == '__main__':
    preprocess()
