from flask import Flask, request
from joblib import load
import yaml

app = Flask(__name__)


config_path = 'params/predict_app.yml'
with open(config_path) as f:
    config = yaml.safe_load(f)


def get_model():
    return load(config['model_path'])


@app.route('/', methods=['POST', 'GET'])
def home():
    f_1 = request.get_json()["GrLivArea"]
    f_1 = int(f_1)
    model = get_model()
    result = model.predict([[f_1]])
    return str(f'{result[0]} RUB')


if __name__ == '__main__':
    app.run(host="0.0.0.0")
