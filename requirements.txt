# external requirements
click
Sphinx
coverage
flake8
pandas
matplotlib
seaborn
scipy
scikit-learn
flask
pyyaml
